<%@ page contentType="text/html; charset=UTF-8" %>
<div class="input-group m-3 w-75">
    <input
      type="text"
      class="form-control"
      id="search-bar"
      placeholder="Busca el evento de tu interés..."
    />
    <button
      class="btn btn-outline-danger"
      type="button"
      id="search-button"
    >
      Buscar
    </button>
  </div>